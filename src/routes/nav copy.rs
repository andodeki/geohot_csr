// use leptos::{component, view, IntoView, Scope};
use leptos::*;
// use leptos_router::*;
use leptos_meta::*;
use leptos_icons::*;

#[component]
pub fn Nav(cx: Scope) -> impl IntoView {

    let (active, setActive) = create_signal(cx, "navBar");

    let showNav = move |_cx: Scope| { setActive.set("navBar activeNavbar") };
    let removeNav = move |_cx: Scope| { setActive.set("navBar") };

    view! { cx,
        <Stylesheet id="leptos" href="/nav.css"/>
        <section class="navBarSection">
            <header class="header flex">
                <div class="logoDiv">
                    <a href="#" class="logo">
                        <h1><Icon class="icon" icon=Icon::from(SiIcon::SiYourtraveldottv) />Tour.</h1>
                        // <h1><SiYourtraveldottv class="icon"/>Tour.</h1>
                    </a>
                </div>
                <div class={ move || active.get()}>
                    <ul class="navLists flex">
                        <li class="navItem">
                            <a href="#" class="navLink">Home</a>
                        </li>
                        <li class="navItem">
                            <a href="#" class="navLink">Packages</a>
                        </li>
                        <li class="navItem">
                            <a href="#" class="navLink">Shop</a>
                        </li>
                        <li class="navItem">
                            <a href="#" class="navLink">About</a>
                        </li>
                        <li class="navItem">
                            <a href="#" class="navLink">Pages</a>
                        </li>
                        <li class="navItem">
                            <a href="#" class="navLink">News</a>
                        </li>
                        <li class="navItem">
                            <a href="#" class="navLink">Contact</a>
                        </li>
                        <button class="btn">
                            <a href="#">BOOK NOW</a>
                        </button>
                    </ul>
                    <div on:click=move |_| removeNav(cx) class="closeNavbar">
                        // <AiFillCloseCircle class="icon"/>
                        <Icon class="icon" icon=Icon::from(AiIcon::AiCloseCircleFilled) />
                    </div>
                </div>
                <div on:click=move |_| showNav(cx) class="toggleNavBar">
                    // <TbGridDots class="icon"/> TbGridDots
                    <Icon class="icon" icon=Icon::from(TbIcon::TbGridDots) />
                </div>
            </header>
        </section>
    }
}
