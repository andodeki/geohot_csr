use leptos::*;

#[component]
pub fn Footer(cx: Scope) -> impl IntoView {
    // let (value, set_value) = create_signal(cx, 0);

    view! { cx,
    <div class="footer" id="footer">
        <div class="logo">
            <img src="/images/favicon.png" alt="logoface" class="logoface"/>
            <a href="/"><h2 class="sitename reversed">Realtime Colors</h2></a>
        </div>
        <div class="footer-cols">
            <div class="footer-col">
                <p style="margin: 0;">Visualize your color choices.</p>
                <a href="https://www.producthunt.com/posts/real-time-colors?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-real&#0045;time&#0045;colors" target="_blank"><img src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=390080&theme=dark" alt="Real&#0045;time&#0032;Colors - Real&#0045;time&#0032;UI&#0032;Color&#0032;Palette&#0032;Tool | Product Hunt" style="width: 250px; height: 54px;" width="250" height="54" /></a>
                // <script type='text/javascript' src='https://storage.ko-fi.com/cdn/widget/Widget_2.js'></script><script type='text/javascript'>kofiwidget2.init('Support Me on Ko-fi', '#221D21', 'Z8Z2L1JGX');kofiwidget2.draw();</script>
            </div>

            <div class="footer-col">
                <div class="reversed sub menu-item-header">EXPLORE</div>
                <a href="https://www.figma.com/community/plugin/1234060894101724457/Realtime-Colors" target="_blank" class="menu-item">Try Figma Plugin
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                        <rect x="0.318182" y="0.318182" width="13.3636" height="13.3636" rx="2.10647" stroke="var(--bg)" stroke-width="0.636364"/>
                        <path d="M4.33594 9.7564L9.75615 4.33618M9.75615 4.33618L5.41998 4.33618M9.75615 4.33618V8.94337" stroke="var(--bg)" stroke-width="0.813033" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
                <a href="/palettes" class="menu-item">Palette Generator</a>
                <a href="/docs" class="menu-item reversed">Docs <span style="background-color: var(--bg); padding: 0.4em 0.4em 0.4em 0.2em; border-radius: 5px; font-size: 12px; color: var(--text);"> New!</span></a>
            </div>

            <div class="footer-col">
                <div class="reversed sub menu-item-header">"LET'S" CONNECT</div>
                <a href="https://twitter.com/juxtopposed" class="menu-item reversed" target="_blank">Twitter</a>
                <a href="https://codepen.io/Juxtopposed" target="_blank" class="menu-item reversed">CodePen</a>
                <a href="https://www.youtube.com/@juxtopposed" target="_blank" class="menu-item reversed">YouTube</a>
                <a href="https://dribbble.com/juxtopposed" target="_blank" class="menu-item reversed">Dribbble</a>
                <a href="https://github.com/juxtopposed" target="_blank" class="menu-item reversed">GitHub</a>
            </div>
        </div>
        <div style="width: 100%; height: 1px; background-color: var(--bg); opacity: 20%; margin-top: 1em;"></div>
        <div>
            <p class="sub">Feedback? Suggestions? Features you 'd like to see? <a href="mailto:juxtopposed.me@gmail.com" target="_blank" class="link reversed">Contact me!</a></p>
            <p class="sub">Made with "<3" by <a href="https://twitter.com/juxtopposed" target="_blank" class="link reversed">@Juxtopposed</a></p>
            <p class="sub">"Copyright © S 2023 All Rights Reserved"</p>
        </div>

    </div>
    }
}
