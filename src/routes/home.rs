use leptos::*;
// use leptos_meta::*;

#[component]
pub fn Home(cx: Scope) -> impl IntoView {
    // let (value, set_value) = create_signal(cx, 0);

    // let (active, setActive) = create_signal(cx, "navBar");

    // let showNav = move |_cx: Scope| { setActive.set("navBar activeNavbar") };
    // let removeNav = move |_cx: Scope| { setActive.set("navBar") };

    view! { cx,
        <div class="hero">
            <div class="hero-text">
                <h1>Visualize Your <span class="color-effect">Colors</span><br/>On a Real Website</h1>
                <p class="subtitle">Choosing a color palette for your website?<br/>Use the Toolbar below to realize your choices.</p>
                <div class="hero-cta">
                    <a href="#toolbar" onclick="highlightToolbar()" class="primary-button">Get Started</a>
                    <a href="#how" class="secondary-button">How does it work?</a>
                </div>
                <div class="hero-scroll">
                    <svg width="23" height="33" viewBox="0 0 23 33" fill="none">
                        <rect x="0.767442" y="0.767442" width="20.7209" height="31.4651" rx="10.3605" stroke="var(--text)" stroke-width="1.53488"/>
                        <rect x="9" y="8" width="4" height="8" rx="2" fill="var(--text)"/>
                    </svg>
                    <p class="sub">Scroll to see more sections</p>
                </div>
            </div>
            <div class="hero-img">
                <a href="#mondrian-info" id="mondrian-info-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                        <path d="M9.06818 14.2259V7.6804H10.8835V14.2259H9.06818ZM9.98011 6.83665C9.71023 6.83665 9.47869 6.74716 9.28551 6.56818C9.09517 6.38636 9 6.16903 9 5.91619C9 5.66619 9.09517 5.4517 9.28551 5.27273C9.47869 5.09091 9.71023 5 9.98011 5C10.25 5 10.4801 5.09091 10.6705 5.27273C10.8636 5.4517 10.9602 5.66619 10.9602 5.91619C10.9602 6.16903 10.8636 6.38636 10.6705 6.56818C10.4801 6.74716 10.25 6.83665 9.98011 6.83665Z" fill="var(--text)"/>
                        <circle cx="10" cy="10" r="9.44118" stroke="var(--text)" stroke-width="1.11765"/>
                    </svg>
                </a>
                <svg xmlns="http://www.w3.org/2000/svg" width="652" height="644" viewBox="0 0 652 644" fill="none" class="mondrian">
                    <rect opacity="0.05" x="1" width="163" height="60" rx="10" fill="var(--text)"/>
                    <rect x="424" width="193" height="60" rx="10" fill="var(--secondary)"/>
                    <rect x="424" y="68" width="193" height="175" rx="10" fill="var(--secondary)"/>
                    <rect opacity="0.2" x="424" y="401" width="193" height="79" rx="10" fill="var(--primary)"/>
                    <rect x="255" y="626" width="362" height="18" rx="9" fill="var(--bg)"/>
                    <rect x="80" y="579" width="166" height="65" rx="10" fill="var(--bg)"/>
                    <rect x="255" y="579" width="160" height="40" rx="10" fill="var(--text)"/>
                    <rect opacity="0.05" x="255" y="490" width="160" height="80" rx="10" fill="var(--text)"/>
                    <rect opacity="0.05" x="255" y="400" width="160" height="80" rx="10" fill="var(--text)"/>
                    <rect x="80" y="68" width="335" height="324" rx="10" fill="var(--primary)"/>
                    <rect x="80" y="401" width="166" height="169" rx="10" fill="var(--text)"/>
                    <rect x="424" y="490" width="193" height="129" rx="10" fill="var(--accent)"/>
                    <rect x="626" y="490" width="26" height="154" rx="10" fill="var(--primary)"/>
                    <rect x="424" y="252" width="91" height="140" rx="10" fill="var(--bg)"/>
                    <rect x="524" y="252" width="93" height="140" rx="10" fill="var(--bg)"/>
                    <rect opacity="0.05" x="626" width="26" height="480" rx="10" fill="var(--text)"/>
                    <rect x="173" width="242" height="60" rx="10" fill="var(--bg)"/>
                    <rect x="1" y="68" width="70" height="157" rx="10" fill="var(--bg)"/>
                    <rect opacity="0.05" x="1" y="234" width="70" height="259" rx="10" fill="var(--text)"/>
                    <rect x="1" y="502" width="70" height="142" rx="10" fill="var(--secondary)"/>
                </svg>

            </div>
        </div>
    }
}
