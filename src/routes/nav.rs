// use leptos::{component, view, IntoView, Scope};
use leptos::*;
// use leptos_router::*;
use leptos_meta::*;
use leptos_icons::*;

#[component]
pub fn Nav(cx: Scope) -> impl IntoView {

    // let (active, setActive) = create_signal(cx, "navBar");

    // let showNav = move |_cx: Scope| { setActive.set("navBar activeNavbar") };
    // let removeNav = move |_cx: Scope| { setActive.set("navBar") };

    view! { cx,
        <Stylesheet id="leptos" href="/nav.css"/>
            <nav>
                <div class="logo">
                    <h1><Icon class="icon" icon=Icon::from(SiIcon::SiYourtraveldottv) /></h1>
                    // <img src="/images/favicon.png" alt="logoface" class="logoface"/>
                    <a href="/"><h2 class="sitename">Tour</h2></a>
                </div>
                <div class="ham-menu">
                    <button id="hamburger">
                        <span></span>
                        <span></span>
                    </button>
                </div>

                <div class="menu hide">

                    <a href="https://www.figma.com/community/plugin/1234060894101724457/Realtime-Colors" target="_blank" class="menu-item menu-item-mobile">Try Figma Plugin
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                            <rect x="0.318182" y="0.318182" width="13.3636" height="13.3636" rx="2.10647" stroke="var(--text)" stroke-width="0.636364"/>
                            <path d="M4.33594 9.7564L9.75615 4.33618M9.75615 4.33618L5.41998 4.33618M9.75615 4.33618V8.94337" stroke="var(--text)" stroke-width="0.813033" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
                    <a href="/palettes" class="menu-item menu-item-mobile">Palette Generator</a>
                    <a href="/docs" class="menu-item menu-item-mobile">Docs <span style="background-color: var(--text); padding: 0.4em 0.4em 0.4em 0.2em; border-radius: 5px; font-size: 12px; color: var(--bg);"> New!</span></a>
                    <a href="#sub" class="primary-button menu-item-font">Subscribe</a>
                </div>
            </nav>
    }
}
