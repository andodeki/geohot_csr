use cfg_if::cfg_if;
use leptos::*;
// use leptos::{component, view, IntoView, Scope};
use leptos_meta::*;
use leptos_router::*;
use routes::{nav::*, home::*, main::*, footer::*};
// mod api;
// pub mod error_template;
// pub mod fallback;
// pub mod handlers;
mod routes;

#[component]
pub fn App(cx: Scope) -> impl IntoView {
    provide_meta_context(cx);

    view! {
        cx,
        <div>
            <Link rel="shortcut icon" type_="image/ico" href="/favicon.ico"/>
            <Stylesheet id="leptos" href="/pkg/geohot_csr.css"/>
            <Meta name="description" content="Geohot."/>
            <Router>
                <Nav />
                <Home />
                <Main />
                <Footer />
            </Router>
        </div>
    }
}

// Needs to be in lib.rs AFAIK because wasm-bindgen needs us to be compiling a lib. I may be wrong.
cfg_if! {
    if #[cfg(feature = "hydrate")] {
        use wasm_bindgen::prelude::wasm_bindgen;

        #[wasm_bindgen]
        pub fn hydrate() {
            _ = console_log::init_with_level(log::Level::Debug);
            console_error_panic_hook::set_once();
            leptos::mount_to_body(move |cx| {
                view! { cx, <App/> }
            });
        }
    }
}
